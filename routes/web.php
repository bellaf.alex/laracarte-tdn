<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::name('home')->get('/','PagesController@home');
Route::view('/','pages.home')->name('home');

//Route::name('about')->get('/about','PagesController@about');
Route::view('/about','pages.about')->name('about');

Route::name('contact')->get('/contact','MessagesController@create');

Route::name('contact')->post('/contact','MessagesController@store');

/* Route::get('/about',[
    'as' => 'about',
    'uses' => 'PagesController@about'
]); */

Auth::routes(['verify' => true] );

