<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Mail\ContactMessageCreated;
use Illuminate\Support\Facades\Mail;
use App\Models\Message;

class MessagesController extends Controller
{
    public function create(){
        return view('messages.contact');
    }
    public function store(ContactRequest $request){
        $message = Message::create($request->only('name','email','message'));

        Mail::to(config('laracarte.admin_support_email'))->send(new ContactMessageCreated($message));

        flashy('Nous vous répondrons dans les plus brefs délais !');
        
        return redirect()->home();
        
    }
}
