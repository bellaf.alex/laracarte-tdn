@extends('errors::layout')
@section('title','404 | Page non trouvée')
@section('message')
    <p>Désolé, Bjorn n'a pas su trouver la page demandée</p>
    <img src="{{asset('/img/Bjorn.png')}}" alt="Bjorn la licorne" style="width:50%">
    <p><a href="{{route('home')}}" style="color:#636b6f">Retourner à la page d'accueil</a></p>
@endsection
    