@extends('layouts.default',['title'=>'Contact'])

@section('content')

<div class="container mr-auto">

    <h2>Get In Touch</h2>

    <p><span class="text-muted">If you having trouble with this service, please <a href="mailto:{{config('laracarte.admin_support_email')}}" data-helpful="laracarte" data-helpful-modal="on">ask for help</a>.</span></p>

    <form method="POST" action="{{route('contact')}}" novalidate>
        @csrf

        <div class="form-group ">
            <label class="control-label" for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control {{$errors->has('name') ? 'is-invalid' : '' }}"required="required" value="{{old('name')}}">
            {!! $errors->first('name','<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group ">
            <label class="control-label" for="email">Email</label>
            <input type="email" name="email" id="email" class="form-control {{$errors->has('email') ? 'is-invalid' : '' }}" required="required" value="{{old('email')}}">
            {!! $errors->first('email','<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group ">
            <label for="message" class="control-label sr-only">Message</label>
            <textarea class="form-control {{$errors->has('message') ? 'is-invalid' : '' }}" name="message" id="message" rows="10" cols="10" required="required" >{{old('message')}}</textarea>
            {!! $errors->first('message','<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-dark btn-block">Submit Enquiry &raquo;</button>
        </div>
    </form>

</div>

    
@endsection