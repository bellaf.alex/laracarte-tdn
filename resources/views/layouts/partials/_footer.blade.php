<footer class="text-center mt-5">
    <p>&copy; {{date('Y')}} &middot; {{config('app.name')}} by <a href="#">Mushi Mushi</a></p>
    <p><b>This app has been built for learning purpose</b></p>
</footer>